import java.util.Arrays;
import java.util.Scanner;

/* Create a program using arrays that sorts a list of integers in descending order. Descending order is highest to lowest value. For example the array 106, 26, 81, 5, 15 your program should sort and display 106, 81, 26, 15, 5.
    Set up the program so that the numbers to sort are read in from the keyboard. Implement the following methods:
    getIntegers, printArray, and sortIntegers
    getIntegers returns an array of integers entered at the keyboard
    printArray prints an array - don't use toString
    sortIntegers should sort the array and return a new array with the sorted numbers
    You will have to figure out how to copy the array elements from the passed array into the new array and sort them and return the new sorted array
 */
public class Main {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] integersToSort = getIntegers();
        int[] sortedArray = sortIntegers(integersToSort);
        printArray(sortedArray);
    }

    private static int[] getIntegers()
    {
        System.out.println("Please enter the size of the array: ");
        int size = sc.nextInt();
        int[] arrayToSort = new int[size];
        for(int i=0; i < arrayToSort.length; ++i) {
            System.out.println("Please enter number " + (i+1) + ":");
            arrayToSort[i] = sc.nextInt();
        }
        return arrayToSort;
    }

    private static void printArray(int[] arrayToPrint)
    {
        //Aiming for [1, 2, 3]
        if(arrayToPrint.length == 0)
        {
            System.out.println("It is strange to ask to sort an empty array. You just wasting my time");
        }
        else {
            System.out.print("[");
        }
        for(int i=0; i < arrayToPrint.length; ++i)
        {
            if(i == (arrayToPrint.length - 1))
            {
                System.out.print(arrayToPrint[i] + "]");
            }
            else
                {
                System.out.print(arrayToPrint[i] + ", ");
            }
        }
    }

    private static int[] sortIntegers(int[] arrayToSort)
    {
        int[] sortedArray = Arrays.copyOf(arrayToSort, arrayToSort.length);
        boolean continueFlag = true;
        int temp;
        while(continueFlag)
        {
            continueFlag = false;
            for(int i=0; i < sortedArray.length-1; ++i)
            {
                if(sortedArray[i] < sortedArray[i+1])
                {
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i+1];
                    sortedArray[i+1] = temp;
                    continueFlag = true;
                }
            }
        }
        return sortedArray;
    }

}
